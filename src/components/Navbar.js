import { useContext } from 'react'
import { Button, Container, Nav, Navbar } from 'react-bootstrap'
import Form from 'react-bootstrap/Form'
import { Link } from 'react-router-dom'
import UserContext from '../UserContext'

// import styles from './Navbar.module.css'

export default function HorizontalNavbar() {
	const user = useContext(UserContext)
	return (
		<>
			<Navbar bg="light" expand="lg">
				<Container fluid>
					<Navbar.Brand>EzPc</Navbar.Brand>
					<Navbar.Toggle aria-controls="navbarScroll" />
					<Navbar.Collapse id="navbarScroll">
						{console.log(user.user.isAdmin)}
						{user.user.isAdmin ? (
							<Nav
								className="me-auto my-2 my-lg-0"
								style={{ maxHeight: '100px' }}
								navbarScroll
							>
								<Nav.Link as={Link} to="/adminProducts">
									PRODUCTS
								</Nav.Link>
								<Nav.Link as={Link} to="/archivedProducts">
									ARCHIVED PRODUCTS
								</Nav.Link>
								<Nav.Link as={Link} to="/createProduct">
									CREATE PRODUCT
								</Nav.Link>
							</Nav>
						) : (
							<Nav
								className="me-auto my-2 my-lg-0"
								style={{ maxHeight: '100px' }}
								navbarScroll
							>
								<Nav.Link as={Link} to="/products">
									HOME
								</Nav.Link>
								<Nav.Link as={Link} to="/history">
									HISTORY
								</Nav.Link>
							</Nav>
						)}

						{/* <Form className="d-flex m-auto w-50">
							<Form.Control
								type="search"
								placeholder="Search"
								aria-label="Search"
							/>
							<Button className="me-5" variant="success">
								Search
							</Button>
						</Form> */}
						{user.user.id ? (
							<Button type="submit" variant="light" as={Link} to="/logout">
								LOGOUT
							</Button>
						) : (
							<>
								<Button type="submit" variant="light" as={Link} to="/login">
									LOGIN
								</Button>
								<Button type="submit" variant="light" as={Link} to="/register">
									SIGNUP
								</Button>
							</>
						)}
					</Navbar.Collapse>
				</Container>
			</Navbar>
		</>
	)
}
