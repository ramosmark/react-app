import { useState } from 'react'
import { Button, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'

export default function TableRow({ data }) {
	const archive = (e, id) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-type': 'application/json',
			},
		})
			.then((res) => res.json())
			.then((data) => {
				console.log(data)
				if (data.success) {
					Swal.fire({
						title: 'Product has been archived',
						icon: 'success',
						text: data.message,
					})
				} else {
					Swal.fire({
						title: 'Archiving product failed',
						icon: 'error',
						text: data.message,
					})
				}
			})
	}

	const unarchive = (e, id) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/unarchive`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-type': 'application/json',
			},
		})
			.then((res) => res.json())
			.then((data) => {
				if (data.success) {
					Swal.fire({
						title: 'Product has been activated',
						icon: 'success',
						text: data.message,
					})
				} else {
					Swal.fire({
						title: 'Product activation failed',
						icon: 'error',
						text: data.message,
					})
				}
			})
	}

	return (
		<tr>
			<td>{data.name}</td>
			<td>{data.description}</td>
			<td>{data.price}</td>
			<td>
				{data.isActive ? (
					<>
						<Button
							className="me-2"
							variant="primary"
							as={Link}
							to={`/editProduct/${data._id}`}
						>
							Edit
						</Button>
						<Button variant="danger me-2" onClick={(e) => archive(e, data._id)}>
							Deactivate
						</Button>
						<Button variant="danger">Delete</Button>
					</>
				) : (
					<>
						<Button
							variant="success me-2"
							onClick={(e) => unarchive(e, data._id)}
						>
							Activate
						</Button>
						<Button variant="danger">Delete</Button>
					</>
				)}
			</td>
		</tr>
	)
}
