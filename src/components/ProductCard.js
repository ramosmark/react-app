import { Row, Col, Card } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function ProductCard({ product }) {
	return (
		<Col xs={6} md={3} className="mt-3">
			<Card as={Link} to={`${product._id}`}>
				<Card.Img
					variant="top"
					src="https://images.pexels.com/photos/1042143/pexels-photo-1042143.jpeg?auto=compress&cs=tinysrgb&w=1600"
				/>
				<Card.Body>
					<Card.Title>{product.name}</Card.Title>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>{product.price}</Card.Text>
				</Card.Body>
			</Card>
		</Col>
	)
}
