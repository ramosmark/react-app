import { useEffect, useState, useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

// import styles from './Login.module.css'

export default function Login() {
	const { user, setUser } = useContext(UserContext)
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {
		if (email && password) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	const loginUser = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-type': 'application/json',
			},
			body: JSON.stringify({
				email: email,
				password: password,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data.success) {
					localStorage.setItem('token', data.access)
					retrieveUserDetails(data.access)

					Swal.fire({
						title: 'Login Successful',
						icon: 'success',
						text: 'Welcome to my EzPc!',
					})
				} else {
					Swal.fire({
						title: 'Login Failed',
						icon: 'error',
						text: data.message,
					})
				}
			})
	}

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`,
			},
		})
			.then((res) => res.json())
			.then((data) => {
				console.log(data)
				setUser({
					id: data.user._id,
					isAdmin: data.user.isAdmin,
				})
			})
	}

	return user.id ? (
		user.isAdmin ? (
			<Navigate to="/adminProducts" />
		) : (
			<Navigate to="/products" />
		)
	) : (
		<>
			<h1 className="text-center">LOGIN</h1>
			<Form onSubmit={(e) => loginUser(e)}>
				<Form.Group className="mb-3" controlId="formBasicEmail">
					<Form.Label>Email address</Form.Label>
					<Form.Control
						type="email"
						value={email}
						onChange={(e) => setEmail(e.target.value)}
						placeholder="Enter Email"
					/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="formBasicPassword">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						value={password}
						onChange={(e) => setPassword(e.target.value)}
						placeholder="Enter Password"
					/>
				</Form.Group>
				<Button
					className=""
					variant="primary"
					type="submit"
					disabled={!isActive}
				>
					Login
				</Button>
			</Form>
		</>
	)
}
