import { useState, useEffect } from 'react'
import { Form, Button } from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function Register() {
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [isActive, setIsActive] = useState(false)

	useEffect(() => {
		if (
			firstName &&
			lastName &&
			email &&
			password1 &&
			password1 === password2
		) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, email, password1, password2])

	const registerUser = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data.success) {
					Swal.fire({
						title: 'Registration is successful',
						icon: 'success',
						text: data.message,
					})
				} else {
					Swal.fire({
						title: 'Registration failed',
						icon: 'error',
						text: data.message,
					})
				}
			})
	}

	return (
		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group className="mb-3" controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					value={firstName}
					onChange={(e) => setFirstName(e.target.value)}
					placeholder="Enter First Name"
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					value={lastName}
					onChange={(e) => setLastName(e.target.value)}
					placeholder="Enter Last Name"
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control
					type="email"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					placeholder="Enter email"
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					value={password1}
					onChange={(e) => setPassword1(e.target.value)}
					placeholder="Password"
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					value={password2}
					onChange={(e) => setPassword2(e.target.value)}
					placeholder="Password"
				/>
			</Form.Group>

			<Button variant="primary" type="submit" disabled={!isActive}>
				SIGNUP
			</Button>
		</Form>
	)
}
