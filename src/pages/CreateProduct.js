import { useState } from 'react'
import { Form, Button } from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function CreateProduct() {
	const [productName, setProductName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')

	const addProduct = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-type': 'application/json',
			},
			body: JSON.stringify({
				name: productName,
				description: description,
				price: Number(price),
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data.success) {
					Swal.fire({
						title: 'Product has been created',
						icon: 'success',
						text: data.message,
					})
				} else {
					Swal.fire({
						title: 'Product creation failed',
						icon: 'error',
						text: data.message,
					})
				}
			})
	}

	return (
		<>
			<h1 className="text-center">Create Product</h1>
			<Form onSubmit={(e) => addProduct(e)}>
				<Form.Group className="mb-3" controlId="productName">
					<Form.Label>Product Name</Form.Label>
					<Form.Control
						type="text"
						value={productName}
						onChange={(e) => setProductName(e.target.value)}
						placeholder="Enter Product Name"
					/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="description">
					<Form.Label>Product Description</Form.Label>
					<Form.Control
						type="text"
						value={description}
						onChange={(e) => setDescription(e.target.value)}
						placeholder="Enter Product Description"
					/>
				</Form.Group>

				<Form.Group className="mb-3" controlId="price">
					<Form.Label>Product Price</Form.Label>
					<Form.Control
						type="text"
						value={price}
						onChange={(e) => setPrice(e.target.value)}
						placeholder="Enter Product Price"
					/>
				</Form.Group>

				{/* <Form.Group className="mb-3" controlId="quantity">
				<Form.Label>Quantity</Form.Label>
				<Form.Control type="text" placeholder="Input Quantity" />
				</Form.Group> */}

				<Button variant="primary" type="submit">
					Submit
				</Button>
			</Form>
		</>
	)
}
