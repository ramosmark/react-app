import { Fragment, useState, useEffect } from 'react'
import { Row, Container } from 'react-bootstrap'
import ProductCard from '../components/ProductCard'

export default function AllProducts() {
	const [products, setProducts] = useState([])

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/`)
			.then((res) => res.json())
			.then((data) => {
				setProducts(data)
			})
	}, [])

	return (
		<Container>
			<Row className="my-2 my-4">
				<h1 className="text-center">Products</h1>
				{products.map((product) => {
					return <ProductCard key={product._id} product={product} />
				})}
			</Row>
		</Container>
	)
}
