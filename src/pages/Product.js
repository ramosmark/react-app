import { useEffect, useState, useContext } from 'react'
import { Container, Button } from 'react-bootstrap'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

import styles from './Product.module.css'

export default function Product() {
	const { user } = useContext(UserContext)
	const id = window.location.pathname.substring(10, 35)
	const [product, setProduct] = useState('')
	const [quantity, setQuantity] = useState(0)
	const [totalPrice, setTotalPrice] = useState(0)

	const addToCart = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-type': 'application/json',
			},
			body: JSON.stringify({
				userId: user.id,
				productId: id,
				quantity: quantity,
			}),
		})
			.then((res) => res.json())
			.then((data) => {
				if (data.success) {
					Swal.fire({
						title: 'Product has been created',
						icon: 'success',
						text: data.message,
					})
				} else {
					Swal.fire({
						title: 'Product creation failed',
						icon: 'error',
						text: data.message,
					})
				}
			})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
			.then((res) => res.json())
			.then((data) => {
				setProduct(data)
			})
	}, [])

	const addQuantity = () => {
		setQuantity(quantity + 1)
	}

	const minusQuantity = () => {
		if (quantity > 0) {
			setQuantity(quantity - 1)
		}
	}

	useEffect(() => {
		if (product.price) {
			setTotalPrice(quantity * product.price)
		}
	}, [quantity])

	return (
		<Container className="text-center my-5">
			<img
				className={`${styles.img}`}
				src="https://images.pexels.com/photos/1042143/pexels-photo-1042143.jpeg?auto=compress&cs=tinysrgb&w=1600"
			></img>
			<h1>{product.name}</h1>
			<h5>{product.description}</h5>
			<Container>
				<Button variant="secondary" onClick={minusQuantity}>
					-
				</Button>
				<h2 className="d-inline mx-1 my-4">{quantity}</h2>
				<Button variant="secondary" onClick={addQuantity}>
					+
				</Button>
				<p>Total Price: {totalPrice}</p>
			</Container>
			<Button variant="primary mx-3 my-3">BUY NOW</Button>
			<Button variant="success mx-3 my-3" onClick={(e) => addToCart(e)}>
				ADD TO CART
			</Button>
		</Container>
	)
}
